import http.server
import json
import logging
import datetime
from deadman_state_machine import deadman_state_machine, receivers
import argparse
import sys
from receiver_parser import HttpPostJsonReceiverParser

class DeadmanHTTPServer(http.server.HTTPServer):

  def __init__(self, server_address, RequestHandlerClass, bind_and_activate=True, logger=None, deadman=None):
    super().__init__(server_address, RequestHandlerClass, bind_and_activate=True)
    self.logger = logger or logging.getLogger(__name__)
    self.set_deadman(deadman)

  def set_deadman(self, deadman):
      if not isinstance(deadman, deadman_state_machine.DeadmanStateMachine):
          print(deadman)
          raise ValueError('deadman must be an instance of deadman.DeadmanStateMachine')
      self.deadman = deadman

  def get_deadman(self):
      return self.deadman


class DeadmanHTTPRequestHandler(http.server.BaseHTTPRequestHandler):

    def do_GET(self):
        if self.path == '/httprobe':
            data = {'code': 200, 'message': 'OK'}
            self.send_response(200)
            self.send_header('Content-length', len(json.dumps(data)))
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            self.wfile.write(json.dumps(data).encode('UTF-8', 'replace'))

            # update state machine
            self.server.logger.debug('httprobe request updated deadman state. Last ping: {}'.format(self.server.get_deadman().get_last_ping()))
            self.server.get_deadman().request()
        else:
            self.send_error(404, message="Not Found")

    def do_POST(self):
        if self.path == '/deadman':

            # requirements to validate payload
            # * unserialisable json

            length = int(self.headers.get('Content-length', 0))
            payload = self.rfile.read(length).decode()
            try:
                payload_object = json.loads(payload)
            except ValueError:
                self.send_error(400, message="Bad request")

            ## FIXME
            ## validate who makes POST request
            #if 'alerts' not in payload_object:
            #    self.send_error(400, message="Bad request")
            #    return False

            data = {'code': 200, 'message': 'OK'}
            self.send_response(200)
            self.send_header('Content-length', len(json.dumps(data)))
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            self.wfile.write(json.dumps(data).encode('UTF-8', 'replace'))

            # update state machine
            self.server.logger.debug('deadman request updated deadman state. Last ping: {}'.format(self.server.get_deadman().get_last_ping()))
            self.server.get_deadman().set_last_ping(datetime.datetime.now())
            self.server.get_deadman().request()

        else:
            self.send_error(404, message="Not Found")



if __name__ == '__main__':

    # parse command line arguments
    parser = argparse.ArgumentParser(description="DeadmanParser")
    parser.add_argument('--bind-address', '-a', default='', help='IP address to bind socket')
    parser.add_argument('--port', '-p', default=8000, help='Port to bind socket')
    parser.add_argument('--debug', '-v', action='store_true', help='Sets log level to DEBUG')
    parser.add_argument('--timeout', '-t', default=60, help='Sets the number of seconds elapsed before considering that the Man is Dead')
    cli = parser.parse_args()

    # setup logger
    log_level = logging.INFO
    if cli.debug:
        log_level = logging.DEBUG
    FORMAT = '%(asctime)-15s %(message)s'
    logging.basicConfig(level=log_level, format=FORMAT)
    logger = logging.getLogger(__name__)

    # settings file is required and must contain a list of receivers
    try:
        from settings import settings
    except ImportError as e:
        logger.error("You must create a settings file. Exiting.")
        sys.exit(1)

    # load default payloads for alerts and resolves
    try:
        alert_dict = settings.alert_dict
        resolve_dict = settings.resolve_dict
    except AttributeError as e:
        logger.error("Settings file must include default alert_dict and resolve_dict. Exiting.")
        sys.exit(1)

    # build receiver list
    http_post_json_parser = HttpPostJsonReceiverParser('HttpPostJsonReceivers', default_alert_dict=alert_dict, default_resolve_dict=resolve_dict, logger=logger)

    receiver_list = http_post_json_parser.parse(receiver_dict=settings.RECEIVERS)

    if not receiver_list:
        logger.error("You must configure at least a receiver in settings.RECEIVERS. Exiting.")
        sys.exit(1)

    # deadman
    # begins alive
    alive_state = deadman_state_machine.AliveState()
    deadman = deadman_state_machine.DeadmanStateMachine(alive_state, timeout=int(cli.timeout), receivers=receiver_list)

    # httpd server
    server_address = (cli.bind_address, int(cli.port))
    Handler = DeadmanHTTPRequestHandler
    httpd = DeadmanHTTPServer(server_address, Handler, deadman=deadman)
    logger.info("Serving at port {}".format(cli.port))
    httpd.serve_forever()
