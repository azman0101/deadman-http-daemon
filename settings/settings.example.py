"""
Default payloads for alerts and resolves to be sent to receivers
"""
alert_dict = {
    'alertname':'DeadMansSwitch',
    'description': 'The Man is Dead',
}

resolve_dict = {
    'alertname':'DeadMansSwitch',
    'description': 'The Man is Ressurrecting',
}

"""
Receiver dictionary
Must contain a list of receiver settings per type of reveicer
"""
RECEIVERS = {
    'HttpPostJsonReceivers': [
        {
            'name': 'receiver one',
            'url': 'http://localhost:8001/ping',
        },
        {
            'name': 'receiver two',
            'url': 'http://localhost:8002/notify',
            'alert_dict': {
                'alertname':'DeadMansSwitch',
                'alerts': [
                    {
                        'status': 'firing',
                        'labels': {
                            'alertname':'DeadMansSwitch',
                            'severity': 'critical',
                        },
                        'annotations': {
                            'description': 'The Man is Dead',
                        },
                    },
                ]
            },
            'resolve_dict': {
                'alertname':'DeadMansSwitch',
                'alerts': [
                    {
                        'status': 'resolved',
                        'labels': {
                            'alertname':'DeadMansSwitch',
                            'severity': 'critical',
                        },
                        'annotations': {
                            'description': 'The Man is Dead',
                        },
                    },
                ]
            },
        },
    ],
}
