import abc
import logging
from deadman_state_machine import receivers

class ReceiverParser(metaclass=abc.ABCMeta):
    """
    Define an interface for encapsulating the behavior associated with a
    particular parser of receivers.
    """
    @abc.abstractmethod
    def parse(self, receiver_dict):
        pass


class HttpPostJsonReceiverParser(ReceiverParser):

    def __init__(self, name, default_alert_dict=None, default_resolve_dict=None, logger=None):
        """
        param name str containing key name to search in receiver dict
        param logger logger instance
        param alert_dict dict default payload
        param resolve_dict dict default payload
        """
        self.name = name
        self.set_default_alert_dict(default_alert_dict)
        self.set_default_resolve_dict(default_resolve_dict)
        self.logger = logger or logging.getLogger(__name__)

    def get_default_alert_dict(self):
        return self.default_alert_dict

    def set_default_alert_dict(self, default_alert_dict):
        """
        Sets default payload to be sent for an alert/resolve if not overriden
        in receiver settings

        param default_alert_dict dict
        """
        if not default_alert_dict:
            default_alert_dict = {}
        self.default_alert_dict = default_alert_dict

    def get_default_resolve_dict(self):
        return self.default_resolve_dict

    def set_default_resolve_dict(self, default_resolve_dict):
        """
        Sets default payload to be sent for an alert/resolve if not overriden
        in receiver settings

        param default_resolve_dict dict
        """
        if not default_resolve_dict:
            default_resolve_dict = {}
        self.default_resolve_dict = default_resolve_dict

    def parse(self, receiver_dict=None):
        """
        Parse a receiver dictionary and returns a list of allowed receivers

        param receiver_dict dict containing reveicers
        """
        if not receiver_dict:
            self.logger.warn("receiver_dict is empty")
            return []

        if self.name not in receiver_dict:
            self.logger.warn("receiver_dict is has no name '{}'".format(self.name))
            return []

        receiver_list = []

        for receiver in receiver_dict.get(self.name, []):
            # ignore empty urls
            if not receiver.get('url', ''):
                self.logger.debug("Empty url ignored")
                continue

            url = receiver.get('url', '')
            try:
                # get defaults...
                alert_dict = self.get_default_alert_dict()
                resolve_dict = self.get_default_resolve_dict()

                # and override alert and resolve dicts if provided
                if receiver['alert_dict']:
                    if 'alertname' not in receiver['alert_dict']:
                        self.logger.warn("alert_dict must define at least a key 'alertname'")
                    else:
                        alert_dict = receiver['alert_dict']
                if receiver['resolve_dict']:
                    if 'alertname' not in receiver['resolve_dict']:
                        self.logger.warn("resolve_dict must define at least a key 'alertname'")
                    else:
                        resolve_dict = receiver['resolve_dict']
            except KeyError as e:
                pass
            # append receiver to list of receivers
            receiver_list.append(receivers.HttpPostJsonReceiver(url, alert_dict=alert_dict, resolve_dict=resolve_dict))

        return receiver_list
