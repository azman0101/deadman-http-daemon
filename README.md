# Deadman Http Server

HTTP Server which uses deadman_state_machine module.

This server handles 2 type of HTTP requests:

* `GET` requests for healtchecks  on path `/httprobe`
* `POST` requests for alerts/resolves

Default Deadman Switch implementation for Prometheus setups sends a
`POST` request continually, actually to /dev/null .

The objective of this project is to implement the other side of Deadman Switch:

* receive `POST` requests from Prometheus continually,
* for every `POST`, update last seen ping,
* and if ping is not updated for a period of time, send an alert to a receiver
  (Relkon notifier) bypassing AlertManager

It's important to receive `GET` requests in a regular basis, because that's the
mecanism used to check last seen ping and transition to a different state if
needed.

For more details, look at deadman_state_machine package's README.md file.


## Using with Docker

To launch `deadman_http_server.py` in a Docker container, follow the steps below.

### 1. Create a `settings.py` file inside `settings` folder. You can see some examples in `settings.example.py`.

### 2. Build the image

```
docker build -t my-deadman-daemon .
```

### 3. Run a container using the image

```
docker run -it --rm --name my-deadman-running my-deadman-daemon
```

Or run it exposing a port and with daemon arguments:

```
docker run -it --rm -p8000:8000 --name my-deadman-running my-deadman-daemon python deadman_http_server.py --timeout 15 --debug
```


## Override alert and resolve payloads

In `settings.RECEIVERS` you can define your list of receivers.

For each receiver, you must provide a dictionary of settings.

In those settings, you can define keys `alert_dict` or `resolve_dict`, with a
dictionary value containing the payload that will be send to your receivers.


## Create a new receiver type

For example a new receiver called `HttpGetReceiver`.

1. Create a new parser `HttpGetReceiverParser` for the receiver in
   `receiver_parser.py`.  Your parser must subclass ReceiverParser class.

2. In `settings.RECEIVERS`, add a new dictionary containing a list of
   HttpGetReceiver settings:

``` RECEIVERS = { ...  'HttpGetReceivers': [ { ... }, { ... }, ], ```

3. Extend `http_server.py` to use your new `HttpGetReceiverParser`
