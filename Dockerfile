FROM python:3.5-slim

RUN apt-get update && \
    apt-get install -y git

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "./deadman_http_server.py" ]
